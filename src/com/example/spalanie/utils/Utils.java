package com.example.spalanie.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.text.Html;
import android.util.Log;
import android.widget.TextView;

public class Utils {
	
	public static final boolean DEBUG = true;
	public static final String TAG = "Artur";
	
	
	/**
	 * Loguje na LogCat'a (konsole) podany komunikat. Logi wy�wietlane s�
	 * jedynie gdy debug = true.
	 * 
	 * @param message
	 *            Komunikat
	 */
	public static void log(String message) {
		log(TAG, message);
	}

	public static void log(String tag, String message) {
		if (DEBUG) {
			if (null == message) {
				message = "null";
			}
			Log.d(tag, message);
		}
	}

	
	public static String getDateFromUnixtime(int seconds) {
	    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	    
	    long miliSeconds = (long)seconds * 1000L;
	    Date d = new Date(miliSeconds);

	    return formatter.format(d);
	} 
	
	public static int getCurrentTimestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		return Integer.parseInt(tsLong.toString());
	}
	
	public static void setTextWithUnit(TextView tv, String value, String units) {
		tv.setText(Html.fromHtml(value +"<small><small> "+units+"</small></small>"));
	}
}
