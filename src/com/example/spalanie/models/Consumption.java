package com.example.spalanie.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.spalanie.utils.DatabaseHandler;
import com.example.spalanie.utils.Utils;

public class Consumption {

	public final static String TABLE_NAME = "consumption";
	
	public final static String ID = "_id";
	public final static String TIMESTAMP = "timestamp";
	public final static String FUEL = "fuel";
	public final static String DISTANCE = "distance";
	public final static String CONSUMPTION = "consumption";
	public final static String DESCRIPTION = "description";

	public float fuel, consumption;
	public int distance;
	public String description;

	public static String getCreateSqlTable() {
		return "CREATE TABLE IF NOT EXISTS `" + TABLE_NAME + "` (" + ID + " INTEGER PRIMARY KEY, " + TIMESTAMP + " INTEGER, "
				+ FUEL + " TEXT, " + DISTANCE + " INTEGER, " + CONSUMPTION + " TEXT, " + DESCRIPTION + " TEXT);";
	}

	public static int getTotalCount(DatabaseHandler db) {
		return (int) DatabaseUtils.queryNumEntries(db.getReadableDatabase(), TABLE_NAME);
	}

	
	public static class STATS_FILEDS {
		public static final int AVERAGE_CONSUMPTION = 0;
		public static final int SUM_DISTANCE = 1;
		public static final int SUM_FUEL = 2;
		public static final int MAX_CONSUMPTION = 3;
		public static final int MIN_CONSUMPTION = 4;
		public static final int MAX_DISTANCE = 5;
	}
	
	public static float[] getStats(DatabaseHandler db) {
		SQLiteDatabase readableDatabase = db.getReadableDatabase();
		
		Cursor c = readableDatabase.rawQuery("SELECT " +
				"AVG("+CONSUMPTION+")," +
				"SUM("+DISTANCE+")," +
				"SUM("+FUEL+")," +
				"MAX("+CONSUMPTION+")," +
				"MIN("+CONSUMPTION+")," +
				"MAX("+DISTANCE+")" +
						" FROM "+ TABLE_NAME, null);
		
		float[] stats = new float[6];
		
		if (c.getCount() > 0) {
			c.moveToNext();
			
			stats[STATS_FILEDS.AVERAGE_CONSUMPTION] = c.getFloat(STATS_FILEDS.AVERAGE_CONSUMPTION);
			stats[STATS_FILEDS.SUM_DISTANCE] = c.getFloat(STATS_FILEDS.SUM_DISTANCE);
			stats[STATS_FILEDS.SUM_FUEL] = c.getFloat(STATS_FILEDS.SUM_FUEL);
			stats[STATS_FILEDS.MAX_CONSUMPTION] = c.getFloat(STATS_FILEDS.MAX_CONSUMPTION);
			stats[STATS_FILEDS.MIN_CONSUMPTION] = c.getFloat(STATS_FILEDS.MIN_CONSUMPTION);
			stats[STATS_FILEDS.MAX_DISTANCE] = c.getFloat(STATS_FILEDS.MAX_DISTANCE);
		}
		
		return stats;
	}

	public static Cursor getCursor(DatabaseHandler db) {
		SQLiteDatabase readableDatabase = db.getReadableDatabase();
		return readableDatabase.query(TABLE_NAME, null, null, null, null, null, ID + " DESC");
	}
	
	public static boolean add(DatabaseHandler db, Consumption entry) {
		SQLiteDatabase writableDatabase = db.getWritableDatabase();

		try {
			ContentValues values = new ContentValues();
			values.put(TIMESTAMP, Utils.getCurrentTimestamp());
			values.put(FUEL, entry.fuel);
			values.put(DISTANCE, entry.distance);
			values.put(CONSUMPTION, entry.consumption);
			values.put(DESCRIPTION, entry.description);

			writableDatabase.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);

		} catch (SQLException e) {
			Utils.log("SQLEXCEPTION: " + e.getMessage());
			return false;
		}
		
		return true;
	}
	
	
	public static void remove(DatabaseHandler db, long removeId) {
		String sql = "DELETE FROM " + TABLE_NAME  + " WHERE " + ID + " = " + removeId;
		SQLiteDatabase writableDatabase = db.getWritableDatabase();
		writableDatabase.execSQL(sql);
	}

}
