package com.example.spalanie;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.spalanie.models.Consumption;
import com.example.spalanie.utils.DatabaseHandler;

public class StatisticsActivity extends ActionBarActivity {

	private LinearLayout table;
	private TableRow.LayoutParams cellLayoutParams;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);

		DatabaseHandler db = DatabaseHandler.getHelper(this);		
		float screenDensity = getResources().getDisplayMetrics().density;
		cellLayoutParams = new android.widget.TableRow.LayoutParams();
		cellLayoutParams.leftMargin = (int) (screenDensity*20f);
		
		int count = Consumption.getTotalCount(db);
		float[] stats = Consumption.getStats(db);
		table = (LinearLayout) findViewById(R.id.list);
		
		
		addNewStatRow("Liczba tankowan", String.valueOf(count));
		addNewStatRow("Srednie spalanie", String.valueOf(stats[Consumption.STATS_FILEDS.AVERAGE_CONSUMPTION]));
		addNewStatRow("Spalone paliwo", String.valueOf(stats[Consumption.STATS_FILEDS.SUM_FUEL]));
		addNewStatRow("Przejechany dystans", String.valueOf(stats[Consumption.STATS_FILEDS.SUM_DISTANCE]));
		addNewStatRow("Minimalne spalanie", String.valueOf(stats[Consumption.STATS_FILEDS.MIN_CONSUMPTION]));
		addNewStatRow("Maksymalne spalanie", String.valueOf(stats[Consumption.STATS_FILEDS.MAX_CONSUMPTION]));
		addNewStatRow("Maksymalny dystans", String.valueOf(stats[Consumption.STATS_FILEDS.MAX_DISTANCE]));
		
	}
	
	private void addNewStatRow(String label, String value) {
		TableRow row = new TableRow(this);
		
		TextView tvLabel = new TextView(this);
		tvLabel.setText(label);
		row.addView(tvLabel);
		
		TextView tvValue = new TextView(this);
		tvValue.setText(value);
		tvValue.setLayoutParams(cellLayoutParams);
		row.addView(tvValue);
		
		table.addView(row);
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
