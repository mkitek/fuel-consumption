package com.example.views;

import com.example.spalanie.utils.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class FuelingGauge extends View {
	
	private static final int VALUE_HORIZONTAL_MARGIN = 25;
	private static final int RULER_TOP = 60;
	private int minFueling = 3, maxFueling = 15;
	private float value = 0;
	private Paint mTextPaintMin;
	private Paint mTextPaintMax;
	private Paint mTextPaintResult;
	private Paint linePaint;
	
	

	public FuelingGauge(Context context) {
		super(context);
		setUp();
	}

	public FuelingGauge(Context context, AttributeSet attr) {
		super(context, attr);
		setUp();
	}

	public FuelingGauge(Context context, AttributeSet attr, int defaultStyles) {
		super(context, attr, defaultStyles);
		setUp();
	}
	
	
	private void setUp() {
		mTextPaintMin = new Paint();
		mTextPaintMin.setColor(Color.GRAY);
		mTextPaintMin.setTextSize(20);
		
		mTextPaintMax = new Paint();
		mTextPaintMax.setColor(Color.GRAY);
		mTextPaintMax.setTextSize(20);
	
		
		mTextPaintResult = new Paint();
		mTextPaintResult.setColor(Color.BLACK);
		mTextPaintResult.setTextSize(20);
		
		
		linePaint = new Paint();
		linePaint.setAntiAlias(true);
		linePaint.setStrokeWidth(1f);
		linePaint.setColor(Color.GRAY);
		linePaint.setStyle(Paint.Style.STROKE);
		linePaint.setStrokeJoin(Paint.Join.ROUND);
	}
	
	public void setValues(int minFueling, int maxFueling, float fueling) {
		this.minFueling = minFueling;
		this.maxFueling = maxFueling;
		this.value = fueling;
		
		this.invalidate();
	}
	
	public void setValues(float fueling) {
		this.value = fueling;
		
		this.invalidate();
	}
	
	

	@Override
	protected void onMeasure(int widthSpec, int heightSpec) {

		int measuredWidth = MeasureSpec.getSize(widthSpec);
//		int measuredHeight = MeasureSpec.getSize(heightSpec);
		int measuredHeight = 80;

		setMeasuredDimension(measuredWidth, measuredHeight);
	}

	
	
	
	@Override
	protected void onDraw(Canvas canvas){

		int width = getMeasuredWidth();

		String minValue = String.valueOf(minFueling),
				maxValue = String.valueOf(maxFueling);
		
		
		canvas.drawText(minValue, 25-2-mTextPaintMin.measureText(minValue), RULER_TOP+7, mTextPaintMin);
		canvas.drawText(maxValue, width-VALUE_HORIZONTAL_MARGIN+2, RULER_TOP+7, mTextPaintMax);
		
		if (value > 0f) {
			Float textWidth = mTextPaintResult.measureText("7");
			canvas.drawText("7", width/2 - textWidth/2, RULER_TOP-15, mTextPaintResult);
		}
		
		
		int gaps = maxFueling-minFueling;
		float gapWidth = (float)(width-(2*VALUE_HORIZONTAL_MARGIN)) / (float)gaps;
		Utils.log("Gap width="+gapWidth);
		for (int i=1; i<gaps; i++) {
			int xPosition = VALUE_HORIZONTAL_MARGIN+ (int)(i*gapWidth);
			canvas.drawLine(xPosition, RULER_TOP-2, xPosition, RULER_TOP+2, linePaint);
		}
		
		canvas.drawLine(VALUE_HORIZONTAL_MARGIN, RULER_TOP, width-VALUE_HORIZONTAL_MARGIN, RULER_TOP, linePaint);
	}

	
}